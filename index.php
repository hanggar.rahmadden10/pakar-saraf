<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>SISTEM PAKAR</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- CSS Libraries -->

    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/components.css">
</head>

<body class="layout-3">
    <div id="app">
        <div class="main-wrapper container">




            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-body">
                        <!-- Soal 1 -->
                        <div class="card" id="card_soal" style="display: block;">
                            <div class="card-header">
                                <h4>Jawab Pertanyaan di Bawah Ini</h4>
                            </div>
                            <div class="card-body">
                                <h5 id="pertanyaan" class="card-header card-header-info h3 text-center">apakah anda susah mengingat nama benda atau tempat?

                                </h5>
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button id="btnGiveCommand" class=" btn btn-success float-right" title="Start">
                                                Jawab iya/tidak</button>
                                            <span id='message'></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="your-quiz-result"></div>
                            </div>
                        </div>
                        <div class="card w-auto centerr" id="card_hasilalzheimer" style="width: 900px; display:none;">
                            <br>
                            <br>
                            <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                            <div class="card-body" style="height: 450px;overflow-y: auto;">
                                <div>

                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <h5><b>Alzheimer</b></h5>
                                            <p>Penyakit Alzheimer adalah penyakit otak yang mengakibatkan penurunan daya ingat, kemampuan berpikir dan bicara, serta perubahan perilaku secara bertahap.
                                                Kondisi ini banyak ditemukan pada orang-orang di atas 65 tahun.</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3">Pengobatan :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <p>Cara pertama yang dilakukan adalah memberikan obat-obatan yang mampu meredakan gejala dengan cara meningkatkan kadar zat kimia otak. Jenis obat-obatan yang diresepkan dokter adalah rivastigmine, donepezil, dan memantine. Obat ini digunakan untuk menangani penyakit Alzheimer pada tahap awal hingga menengah. Memantine juga dapat diresepkan pada pederita Alzheimer dengan gejala yang sudah memasuki tahap akhir.</p>

                                            <p>Selain pemberian obat-obatan, psikoterapi juga dapat dilakukan untuk menangani penyakit Alzheimer. Terapi ini terdiri dari:</P>
                                            <ul type="disc">
                                                <li>Stimulasi kognitif, yang bertujuan untuk meningkatkan daya ingat, kemampuan berkomunikasi, dan kemampuan dalam memecahkan masalah.</li>
                                                <li>Terapi relaksasi dan terapi perilaku kognitif, yang bertujuan untuk mengurangi halusinasi, delusi, kecemasan, atau depresi yang dialami oleh penderita.</li>
                                            </ul>
                                            <p>Sampai saat ini, belum ada penanganan khusus yang terbukti efektif untuk menyembuhkan penyakit Alzheimer. Upaya penanganan yang dilakukan hanya bertujuan untuk meredakan gejala, memperlambat perkembangan penyakit, serta memampukan penderita untuk hidup semandiri mungkin.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="your-quiz-result"></div>
                                <div class="col-md-4 ml-auto mr-auto text-center">
                                    <br>
                                    <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                                </div>
                            </div>
                        </div>
                        <div class="card w-auto centerr" id="card_hasilmeningitis" style="width: 900px; display:none;">
                            <br>
                            <br>
                            <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                            <div class="card-body" style="height: 450px;overflow-y: auto;">
                                <div>

                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <h5><b>Meningitis</b></h5>
                                            <p>Meningitis adalah peradangan yang terjadi pada meningen, yaitu lapisan pelindung yang menyelimuti otak dan saraf tulang belakang. Meningitis terkadang sulit dikenali, karena penyakit ini memiliki gejala awal yang serupa dengan flu, seperti demam dan sakit kepala.</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3">Pengobatan :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <p>Pengobatan meningitis umumnya berbeda-beda tergantung kepada penyebabnya.
                                                Sebagai contoh, dokter bisa meresepkan obat antimikroba, atau menjalankan terapi lain bila meningitis disebabkan oleh kanker atau lupus.</p>

                                            <p> Penyakit ini bisa dicegah dengan menjalani gaya hidup sehat dan menghindari kondisi yang dapat memicu penyebaran infeksi. Guna meningkatkan kekebalan tubuh dari kuman penyebab meningitis, lakukan vaksinasi (termasuk vaksin PCV) sesuai anjuran dokter.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="your-quiz-result"></div>
                                <div class="col-md-4 ml-auto mr-auto text-center">
                                    <br>
                                    <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                                </div>
                            </div>
                        </div>
                        <div class="card w-auto centerr" id="card_hasilepilepsi" style="width: 900px; display:none;">
                            <br>
                            <br>
                            <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                            <div class="card-body" style="height: 450px;overflow-y: auto;">
                                <div>

                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <h5><b>Epilepsi</b></h5>
                                            <p>Penyakit epilepsi atau ayan adalah gangguan sistem saraf pusat akibat pola aktivitas listrik otak yang tidak normal.
                                                Hal itu menimbulkan keluhan kejang, sensasi dan perilaku yang tidak biasa, hingga hilang kesadaran.</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3">Diagnosis Epilepsi :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <p>Diagnosis epilepsi dapat ditetapkan setelah dokter melakukan pemeriksaan fisik, terutama kondisi saraf pasien, serta serangkaian tes untuk memastikan kondisi yang abnormal pada otak.
                                                Setelah epilepsi terdiagnosis, penting untuk memulai pengobatan secepatnya, dengan pengaturan pola makan dan pemberian obat.</p>
                                        </div>
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3">Pengobatan Epilepsi :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <p>Pemberian obat secara tepat dapat menstabilkan aktivitas listrik dalam otak, serta dapat mengendalikan kejang pada penderita epilepsi. Obat untuk menangani epilepsi adalah obat jenis antiepilepsi.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="your-quiz-result"></div>
                                <div class="col-md-4 ml-auto mr-auto text-center">
                                    <br>
                                    <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                                </div>
                            </div>
                        </div>
                        <div class="card w-auto centerr" id="card_hasilstroke" style="width: 900px; display:none;">
                            <br>
                            <br>
                            <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                            <div class="card-body" style="height: 450px;overflow-y: auto;">
                                <div>

                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <h5><b>Stroke</b></h5>
                                            <p>Stroke adalah kondisi yang terjadi ketika pasokan darah ke otak terganggu atau berkurang akibat penyumbatan (stroke iskemik) atau pecahnya pembuluh darah (stroke hemoragik).
                                                Tanpa darah, otak tidak akan mendapatkan asupan oksigen dan nutrisi, sehingga sel-sel pada sebagian area otak akan mati.</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3">Pengobatan :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <p>Pengobatan stroke tergantung kepada kondisi yang dialami pasien. Dokter dapat memberikan obat-obatan atau melakukan operasi. </p>
                                            <p>Sedangkan untuk memulihkan kondisi, pasien akan dianjurkan menjalani fisioterapi, dan diikuti terapi psikologis apabila diperlukan.</p>

                                            <p>Untuk mencegah stroke, dokter menyarankan untuk:</o>
                                                <br></br>
                                                <ul type="circle">
                                                    <li> Menerapkan pola makan yang sehat.</li>
                                                    <li> Berolahraga secara rutin.</li>
                                                    <li> Hindari merokok dan mengonsumsi minuman keras.</li>
                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="your-quiz-result"></div>
                                <div class="col-md-4 ml-auto mr-auto text-center">
                                    <br>
                                    <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                                </div>
                            </div>
                        </div>
                        <div class="card w-auto centerr" id="card_hasilmultiplesclerosis" style="width: 900px; display:none;">
                            <br>
                            <br>
                            <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                            <div class="card-body" style="height: 450px;overflow-y: auto;">
                                <div>

                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <h5><b>Multiple Sclerosis</b></h5>
                                            <p>Penyakit sklerosis ganda atau multiple sclerosis adalah gangguan saraf pada otak, mata, dan tulang belakang. Multiple sclerosis akan menimbulkan gangguan pada penglihatan dan gerakan tubuh.</p>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="title text-secondary mb-1 mt-3"> Pengobatan untuk meredakan gejala multiple sclerosis :</h3>
                                        </div>
                                        <div class="col-12" id="content_hasil">
                                            <p>Beberapa bentuk pengobatan yang dapat diberikan oleh dokter untuk meredakan gejala multiple sclerosis (MS) adalah:</p>
                                            <ul type="disc">
                                                <li><b> Obat-obatan </b></li>
                                                <p>Dokter dapat memberikan obat kortikosteroid, seperti prednisone dan methylprednisolone, untuk mengurangi peradangan saraf akibat multiple sclerosis.</p>
                                                <p>Selain itu, untuk mengurangi kaku pada otot dokter dapat memberikan obat pelemas otot, seperti baclofen dan tizanidine, serta obat methylphenidate dan obat antidepresan untuk mengurangi rasa lelah.</p>
                                            </ul>
                                            <ul type="disc">
                                                <li><b> Fisioterapi </b></li>
                                                <p>Terapi fisik dan terapi okupasi dilakukan untuk meningkatkan kekuatan fisik pada penderita multiple sclerosis.</p>
                                                <p>Hal ini akan memudahkan penderita MS menjalani kesehariannya.</p>
                                            </ul>
                                            <ul type="disc">
                                                <li><b> Plasmapheresis </b></li>
                                                <p>Dokter akan membuang plasma darah yang ada di dalam tubuh pasien, menggunakan alat khusus.</p>
                                                <p>Untuk mengganti plasma yang dibuang, dokter akan memasukkan cairan infus khusus, seperti albumin.</p>
                                            </ul>
                                            <div class="col-12">
                                                <h3 class="title text-secondary mb-1 mt-3"> Pengobatan untuk mencegah kekambuhan penyakit multiple sclerosis </h3>
                                            </div>
                                            <div class="col-12" id="content_hasil">
                                                <p>Pengobatan ini dilakukan untuk menangani multiple sclerosis yang kambuh. Dokter dapat memberikan suntikan interferon beta untuk mengurangi frekuensi dan keparahan dari kambuhnya multiple sclerosis.</p>
                                                <p>Selain memberikan beta interferon, ada obat lain yang juga dapat digunakan untuk mengurangi kekambuhan multiple sclerosis, yaitu fingolimod. Obat ini diminum sekali sehari.</P>
                                                <p>Beberapa penderita multiple sclerosis yang hanya mengalami gejala ringan tidak perlu mendapatkan pengobatan khusus. Namun, perlu diketahui bahwa sebenarnya belum ada obat yang dapat menyembuhkan multiple sclerosis.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="your-quiz-result"></div>
                                    <div class="col-md-4 ml-auto mr-auto text-center">
                                        <br>
                                        <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card w-auto centerr" id="card_hasilbellspalsy" style="width: 900px; display:none;">
                                <br>
                                <br>
                                <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                                <div class="card-body" style="height: 450px;overflow-y: auto;">
                                    <div>

                                        <div class="row">
                                            <div class="col-12">
                                                <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                            </div>
                                            <div class="col-12" id="content_hasil">
                                                <h5><b>Bell’s Palsy</b></h5>
                                                <p>Bell’s palsy adalah kelumpuhan pada otot wajah yang menyebabkan salah satu sisi wajah tampak melorot.
                                                    Kondisi ini dapat muncul secara tiba-tiba, namun biasanya tidak bersifat permanen.</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <h3 class="title text-secondary mb-1 mt-3">Diagnosis Bell’s Palsy</h3>
                                            </div>
                                            <div class="col-12" id="content_hasil">
                                                <p>Diagnosis dilakukan dokter dengan melakukan pemeriksan gerakan wajah penderita.
                                                    Di samping pemeriksaan fisik, serangkaian pemeriksaan lanjutan akan dilakukan,
                                                    seperti tes darah, elektromiografi, serta pemindaian dengan CT scan dan MRI untuk mengetahui penyebab kelumpuhan otot wajah.</p>
                                            </div>
                                            <div class="col-12">
                                                <h3 class="title text-secondary mb-1 mt-3">Terapi Bell’s Palsy</h3>
                                            </div>
                                            <div class="col-12" id="content_hasil">
                                                <p>Terapi Bell’s palsy bertujuan untuk mempercepat penyembuhan dan mencegah timbulnya komplikasi.
                                                    Terapi tersebut dilakukan dengan cara mengonsumsi obat-obatan dari dokter dan fisioterapi.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="your-quiz-result"></div>
                                    <div class="col-md-4 ml-auto mr-auto text-center">
                                        <br>
                                        <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card w-auto centerr" id="card_tidakadahasil" style="width: 900px; display:none;">
                                <br>
                                <br>
                                <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                                <div class="card-body" style="height: 450px;overflow-y: auto;">
                                    <div>

                                        <div class="row">
                                            <div class="col-12">
                                                <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                            </div>
                                            <div class="col-12" id="content_hasil">
                                                <p>Sistem Tidak Dapat Mendiagnosa Penyakit</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="your-quiz-result"></div>
                                    <div class="col-md-4 ml-auto mr-auto text-center">
                                        <br>
                                        <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                                    </div>
                                </div>
                            </div>


                </section>

            </div>
            <script>
                // soal 1
                var message = document.querySelector('#message');

                var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
                var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;

                var grammar = '#JSGF V1.0;'

                var recognition = new SpeechRecognition();
                var speechRecognitionList = new SpeechGrammarList();
                speechRecognitionList.addFromString(grammar, 1);
                recognition.grammars = speechRecognitionList;
                recognition.lang = 'en-US';
                recognition.interimResults = false;

                recognition.onresult = function(event) {
                    var last = event.results.length - 1;
                    var command = event.results[last][0].transcript;
                    // message.textContent = 'Voice Input: ' + command + '.';

                    if (command.toLowerCase() == 'yes') {
                        pertanyaan2();
                    } else if (command.toLowerCase() == 'no') {
                        pertanyaan8();
                    } else {
                        message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                    }
                };

                recognition.onspeechend = function() {
                    recognition.stop();
                };

                recognition.onerror = function(event) {
                    message.textContent = 'Error occurred in recognition: ' + event.error;
                }

                document.querySelector('#btnGiveCommand').addEventListener('click', function() {
                    recognition.start();

                });

                function pertanyaan2() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda merasa lemas atau kurang berenergi ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        if (command.toLowerCase() == 'yes') {
                            pertanyaan3();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan4();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan3() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML = "Apakah anda mengalami masalah dalam berkomunikasi ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan4();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan4();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan4() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda sering gelisah ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan5();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan8();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan5() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda mulai mengalami halusinasi atau delusi ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan6();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan8();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan6() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah penurunan daya ingat anda semakin parah ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan7();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan8();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan7() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda sulit bergerak tanpa bantuan orang lain ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            var a = document.getElementById("card_hasilalzheimer");
                            var z = document.getElementById("card_soal");
                            if (a.style.display = "none") {
                                $(a.style.display = "block");
                                $(z.style.display = "none");
                            }
                        } else if (command.toLowerCase() == 'no') {
                            tidakadahasil();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan8() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda mengalami demam tinggi ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan9();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan20();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan9() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda mengalami kejang - kejang ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan10();
                        } else if (command.toLowerCase() == 'no') {
                            var a = document.getElementById("card_tidakadahasil");
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan10() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda mengalami sakit kepala ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan11();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan11() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah leher anda terasa kaku ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan12();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan12() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda merasa sensitif terhadap cahaya ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan13();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan13() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda mengalami mual dan muntah ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan14();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan14() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda sulit melakukan konsentrasi atau merasa kebingungan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan15();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan15() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah nafsu makan anda berkurang ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            var a = document.getElementById("card_hasilmeningitis");
                            var z = document.getElementById("card_soal");
                            if (a.style.display = "none") {
                                $(a.style.display = "block");
                                $(z.style.display = "none");
                            }
                        } else if (command.toLowerCase() == 'no') {
                            var a = document.getElementById("card_tidakadahasil");
                            tidakadahasil();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan16() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda merasa trauma dibagian kepala ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan17();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan17() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah gula darah anda rendah ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan18();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan18() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah mata terbuka saat anda kejang - kejang ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan19();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan19() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda kesulitan bernafas ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            var a = document.getElementById("card_hasilepilepsi");
                            var z = document.getElementById("card_soal");
                            if (a.style.display = "none") {
                                $(a.style.display = "block");
                                $(z.style.display = "none");
                            }
                        } else if (command.toLowerCase() == 'no') {
                            var a = document.getElementById("card_tidakadahasil");
                            tidakadahasil();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan20() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda sering pingsan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan21();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan21() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda sering kehilangan kesadaran";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan22();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan22() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda mengalami kelumpuhan tiba - tiba dibagian wajah, tangan, atau kaki ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan23();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan23() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda kesulitan dalam berjalan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan24();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan24() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda mengalami gangguan keseimbangan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan25();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan25() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda sulit untuk mengendalikan emosi ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            var a = document.getElementById("card_hasilstroke");
                            var z = document.getElementById("card_soal");
                            if (a.style.display = "none") {
                                $(a.style.display = "block");
                                $(z.style.display = "none");
                            }
                        } else if (command.toLowerCase() == 'no') {
                            var a = document.getElementById("card_tidakadahasil");
                            tidakadahasil();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan26() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda terasa sangat lelah atau kelelahan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan27();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan27() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda terasa lemas pada bagian tangan atau kaki ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan28();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan28() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah otak anda sering mengalami kejang atau kaku ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan29();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan29() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda mengalami gangguan penglihatan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan30();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan30() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah cara bicara anda tidak jelas ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan31();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan31() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah anda merasakan nyeri pada bagian tubuh ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan32();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan32() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah timbul masalah pada kantung kemih atau pencernaan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan33();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan33() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah Timbul gangguan pada kemampuan motorik dan keseimbangan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            var a = document.getElementById("card_hasilmultiplesclerosis");
                            var z = document.getElementById("card_soal");
                            if (a.style.display = "none") {
                                $(a.style.display = "block");
                                $(z.style.display = "none");
                            }
                        } else if (command.toLowerCase() == 'no') {
                            var a = document.getElementById("card_tidakadahasil");
                            tidakadahasil();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan34() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah timbul masalah pada kantung kemih atau pencernaan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan35();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan35() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah timbul masalah pada kantung kemih atau pencernaan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan36();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan36() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah timbul masalah pada kantung kemih atau pencernaan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan37();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan37() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah timbul masalah pada kantung kemih atau pencernaan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan38();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan38() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah timbul masalah pada kantung kemih atau pencernaan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan39();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan39() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah timbul masalah pada kantung kemih atau pencernaan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            pertanyaan40();
                        } else if (command.toLowerCase() == 'no') {
                            pertanyaan16();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }

                function pertanyaan40() {
                    message.textContent = '';
                    document.getElementById("pertanyaan").innerHTML =
                        "Apakah timbul masalah pada kantung kemih atau pencernaan ?";
                    recognition.onresult = function(event) {
                        var last = event.results.length - 1;
                        var command = event.results[last][0].transcript;
                        var command2 = event.results[last][0].transcript;

                        if (command.toLowerCase() == 'yes') {
                            var a = document.getElementById("card_hasilbellspalsy");
                            var z = document.getElementById("card_soal");
                            if (a.style.display = "none") {
                                $(a.style.display = "block");
                                $(z.style.display = "none");
                            }
                        } else if (command.toLowerCase() == 'no') {
                            var a = document.getElementById("card_tidakadahasil");
                            tidakadahasil();
                        } else {
                            message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                        }
                    };
                }
            </script>
            <footer class="main-footer">
                <div class="footer-left">
                    Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a>
                </div>
                <div class="footer-right">
                    2.3.0
                </div>
            </footer>
        </div>
    </div>

    <!--   Core JS Files   -->
    <script src="asset_user/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="asset_user/js/core/popper.min.js" type="text/javascript"></script>
    <script src="asset_user/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
    <script src="asset_user/js/plugins/moment.min.js"></script>
    <!-- General JS Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="assets/js/stisla.js"></script>

    <!-- JS Libraies -->

    <!-- Page Specific JS File -->

    <!-- Template JS File -->
    <script src="assets/js/scripts.js"></script>
    <script src="assets/js/custom.js"></script>
</body>

</html>