<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="asset_user/img/apple-icon.png">
    <link rel="icon" href="asset_user/img/pakar.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Sistem Pakar Penyakit Mata</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">



    <link rel="stylesheet" type="text/css" href="slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" />

    <link href="asset_user/css/material-kit.css?v=2.0.7" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="asset_user/demo/demo.css" rel="stylesheet" />




    <script src="js/jquery-3.5.1.js"></script>
    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                    "simple-line-icons"
                ],
                urls: ["assets/css/fonts.min.css"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>


    <style>
        .centerr {
            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%)
        }
    </style>
</head>

<body>
    <div id="app">
        <main class="w-100 h-100" id="particles-js" style="position: absolute;">
            <div class="container" style="opacity: 0.8;">
                <div class="divs">
                    <!-- card 1 -->
                    <div class="card w-auto centerr" id="card_soal1" style="width: 90%; display:block;">
                        <div class="card-header card-header-info h3 text-center">Apakah Pada siang hari penglihatan menurun ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes float-right" onclick="yes1()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no float-left" onclick="no1()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 2 -->
                    <div class="card w-auto centerr" id="card_soal2" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Mata anda merasakan nyeri hebat ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes2()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no2()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 3 -->
                    <div class="card w-auto centerr" id="card_soal3" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Mata silau jika terkena cahaya ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes3()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no3()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 4 -->
                    <div class="card w-auto centerr" id="card_soal4" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah anda Sering ganti kacamata ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes4()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no4()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 5 -->
                    <div class="card w-auto centerr" id="card_soal5" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Penglihatan ganda pada salah satu sisi mata ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes5()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no5()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 6 -->
                    <div class="card w-auto centerr" id="card_soal6" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Lensa mata membengkak ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes6()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no6()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 7 -->
                    <div class="card w-auto centerr" id="card_soal7" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Pada Malam hari kesulitan melihat ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes7()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no7()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 8 -->
                    <div class="card w-auto centerr" id="card_soal8" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Mata berair ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes8()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no8()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 9 -->
                    <div class="card w-auto centerr" id="card_soal9" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Mata Anda meradang ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes9()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no9()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 10 -->
                    <div class="card w-auto centerr" id="card_soal10" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Mata mempersempit, perubahan bentuk ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes10()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no10()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 11 -->
                    <div class="card w-auto centerr" id="card_soal11" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Peka terhadap cahaya ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes11()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no11()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 12 -->
                    <div class="card w-auto centerr" id="card_soal12" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah ada Benjolan pada mata bagian atas atau bawah ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes12()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no12()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 13 -->
                    <div class="card w-auto centerr" id="card_soal13" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Mata gatal ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes13()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no13()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 14 -->
                    <div class="card w-auto centerr" id="card_soal14" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Air mata berlebihan ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes14()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no14()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 15 -->
                    <div class="card w-auto centerr" id="card_soal15" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Mata membengkak ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes15()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no15()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 16 -->
                    <div class="card w-auto centerr" id="card_soal16" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Sumber cahaya akan berwarna pelangi bila memandang lampu neon ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes16()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no16()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 17 -->
                    <div class="card w-auto centerr" id="card_soal17" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Penglihatan kabur ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes17()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no17()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 18 -->
                    <div class="card w-auto centerr" id="card_soal18" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Berbentuk keropeng pada kelopak mata ketika bangun pada siang hari ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes18()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no18()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 19 -->
                    <div class="card w-auto centerr" id="card_soal19" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Menekan kedipan berlebihan ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes19()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no19()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 20 -->
                    <div class="card w-auto centerr" id="card_soal20" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Penglihatan yang tadinya kabur lamakelamaan menjadi normal ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes20()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no20()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 21 -->
                    <div class="card w-auto centerr" id="card_soal21" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Sakit kepala ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes21()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no21()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>
                    <!-- card 22 -->
                    <div class="card w-auto centerr" id="card_soal22" style="width: 90%; display:none;">
                        <div class="card-header card-header-info h3 text-center">Apakah Mata perih ?
                        </div>
                        <div class="card-body">
                            <div class="cls">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-info yes2 float-right" onclick="yes22()">
                                                YA
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-dark no2 float-left" onclick="no22()">
                                                TIDAK
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                        </div>
                    </div>

                    <div class="card w-auto centerr" id="card_hasilkatarak" style="width: 900px; display:none;">
                        <br>
                        <br>
                        <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                        <div class="card-body" style="height: 450px;overflow-y: auto;">
                            <div>

                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <h5><b>Katarak</b></h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Pengobatan :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <h5><b>incision cataract surgery (phacoemulsification)</b></h5>
                                        <p>Operasi ini dilakukan dengan melakukan insisi kecil pada tepi kornea.
                                            Selanjutnya, dokter akan menyinarkan gelombang ultrasound untuk menghancurkan
                                            lensa lalu diambil menggunakan alat penghisap.</p>
                                        <h5><b>surgery</b></h5>
                                        <p>Operasi ini membutuhkan insisi yang lebih besar untuk mengeluarkan inti lensa yang berkabut.
                                            Selanjutnya, sisa lensa dikeluarkan dengan menggunakan alat penghisap.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                            <div class="col-md-4 ml-auto mr-auto text-center">
                                <br>
                                <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                            </div>
                        </div>
                    </div>
                    <div class="card w-auto centerr" id="card_hasiloveitis" style="width: 900px; display:none;">
                        <br>
                        <br>
                        <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                        <div class="card-body" style="height: 450px;overflow-y: auto;">
                            <div>

                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <p>Uveitis</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Pengobatan :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <p>Pengobatan uveitis dilakukan untuk meredakan peradangan yang terjadi di dalam mata.
                                            Sebagai langkah awal, dokter mungkin akan meresepkan tetes mata kortikosteroid untuk menghentikan inflamasi.
                                            Andaikan cara ini tidak berhail, mungkin dokter akan mencoba meresepkan kortikosteroid dalam bentuk pil atau obat suntik.</p>

                                        <p>kortikosteroid bisa juga disertai dengan obat-obatan antibiotik, antivirus, atau antijamur apabila
                                            peradangan yang terjadi dipicu oleh infeksi. Andaikan gejala uveitis dinilai cukip parah, atau bisa
                                            membahayakan penglihatan, mungkin dokter akan meresepken obat-obatan penekan sistem imun. Obat ini diberikan bila uveitis tak mempan terhadap kortikosteroid.</p>

                                        <p>Cepat atau lamanya pemulihan eveitis ini bergantung pada berbagai hal. Misalnya, letak atau posisi
                                            radang pada uvea, atau tingkat keparahan gejalanya. Cotonhnya, bila uveitis terjadi di mata bagian
                                            depan, umumnya pemulihannya bisa lecih cepat ketimbang terjadi di belakang mata.</p>

                                        <p>Pada kasus yang jarang terjadi, yang mana uveitis menunjukkan gejala yang sudah sangat parah,
                                            prosedur operasi mungkin bisa dilakukan. Misalnya, seperti operasi untuk memasang alat di dalam
                                            mata yang bisa melepaskan obat secara berkala. Ada pula operasi vitrektomi untuk menghilangkan caairan vitreous di dalam mata.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                            <div class="col-md-4 ml-auto mr-auto text-center">
                                <br>
                                <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                            </div>
                        </div>
                    </div>
                    <div class="card w-auto centerr" id="card_hasilkonjungtivitis" style="width: 900px; display:none;">
                        <br>
                        <br>
                        <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                        <div class="card-body" style="height: 450px;overflow-y: auto;">
                            <div>

                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <p>Konjungtivitis</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Pengobatan :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <p>Pengobatan konjungtivitis berbeda-beda tergantung penyebabnya. Konjungtivitis bakteri diatasi dengan
                                            obat tetes mata atau salep mata antibiotik, sedangkan konjungtivitis alergi diatasi dengan obat antialergi.</p>

                                        <p>Sementara untuk konjungtivitis virus, tidak diperlukan pengobatan khusus karena akan sembuh dengan sendirinya.
                                            Akan tetapi, dokter dapat memberikan obat tetes mata untuk meredakan gejala yang dialami penderita.
                                            Untuk meredakan gejala konjungtivitis, penderita juga dapat menggunakan cara alami untuk mengobati sakit mata,
                                            seperti mengompres mata dengan air hangat atau air dingin.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                            <div class="col-md-4 ml-auto mr-auto text-center">
                                <br>
                                <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                            </div>
                        </div>
                    </div>
                    <div class="card w-auto centerr" id="card_hasilglaukoma" style="width: 900px; display:none;">
                        <br>
                        <br>
                        <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                        <div class="card-body" style="height: 450px;overflow-y: auto;">
                            <div>

                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <p>Glaukoma</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Pengobatan :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <p>1. Obat tetes mata untuk pengidap glaukoma. Penggunaan obat tetes mata baik dilakukan untuk mengurangi pembentukan
                                            cairan pada mata karena adanya penekanan. Namun, penggunaan obat tetes mata ini mempunyai efek samping kemerahan
                                            pada mata, alergi, iritasi mata, dan penglihatan kabur.</p>

                                        <p>2. Operasi. Operasi ini dapat ditempuh ketika kasus-kasus yang terjadi sudah tidak lagi dapat disembuhkan dengan obat-obatan.
                                            Operasi yang dilakukan biasanya berlangsung selama 45-75 menit.</p>
                                        <p>3. Laser. Ada dua jenis laser yang dapat dilakukan sebagai langkah pengobatan glaukoma, yaitu trabekuloplasti dan iridotomi.
                                            Trabekuloplasti merupakan tindakan yang biasa dilakukan untuk orang yang mengidap glaukoma sudut terbuka. Sedangkan iridotomi
                                            merupakan tindakan yang dilakukan untuk pengidap glaukoma sudut tertutup.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                            <div class="col-md-4 ml-auto mr-auto text-center">
                                <br>
                                <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                            </div>
                        </div>
                    </div>
                    <div class="card w-auto centerr" id="card_hasilmiopi" style="width: 900px; display:none;">
                        <br>
                        <br>
                        <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                        <div class="card-body" style="height: 450px;overflow-y: auto;">
                            <div>

                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <p>Miopi (Rabun Jauh)</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Pengobatan :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <h5>Penggunaan kacamata atau lensa kontak</h5>
                                        <p>Langkah penanganan miopi atau rabun jauh yang paling sederhana dan terjangkau adalah dengan menggunakan kacamata atau lensa kontak.
                                            Pemilihan kacamata serta lensa kontak tergantung pada kebutuhan serta kenyamanan pasien.
                                            Ketika memilih menggunakan lensa kontak, pastikan untuk selalu menjaga kebersihan lensa kontak agar terhindar dari infeksi mata.
                                            Lensa kontak juga sebaiknya dilepas sebelum tidur.</p>
                                        <h5>Operasi dengan sinar laser (LASIK)</h5>
                                        <p>Proses operasi dengan sinar laser (LASIK) juga dapat menjadi alternatif. Hampir seluruh pasien yang menjalani operasi ini
                                            merasakan perubahan yang signifikan. Dalam operasi ini, sinar laser akan digunakan untuk mengatur lengkungan kornea.
                                            Perlu diingat, prosedur ini tidak cocok untuk penderita di bawah 21 tahun karena mata mereka masih dapat berkembang.</p>
                                        <h5>Obat tetes mata atropin</h5>
                                        <p>Obat tetes mata atropin diduga dapat mencegah miopi atau rabun jauh yang diderita bertambah parah. Obat tetes mata dapat digunakan
                                            secara rutin pada penderita rabun jauh sesuai dengan resep dokter.</p>
                                        <h5>Implan lensa buatan</h5>
                                        <p>Implan lensa buatan dilakukan untuk menangani miopi atau rabun jauh dengan tingkat keparahan tinggi yang tidak bisa
                                            ditangani dengan operasi laser. Prosedur ini dilakukan dengan memasukkan lensa buatan tanpa mengeluarkan lensa mata
                                            yang asli atau mengganti lensa asli dengan lensa buatan.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                            <div class="col-md-4 ml-auto mr-auto text-center">
                                <br>
                                <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                            </div>
                        </div>
                    </div>
                    <div class="card w-auto centerr" id="card_tidakadahasil" style="width: 900px; display:none;">
                        <br>
                        <br>
                        <div class="card-header card-header-info h3 text-center">Hasil Diagnosa</div>
                        <div class="card-body" style="height: 450px;overflow-y: auto;">
                            <div>

                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                                    </div>
                                    <div class="col-12" id="content_hasil">
                                        <p>Sistem Tidak Dapat Mendiagnosa Penyakit</p>
                                    </div>
                                </div>
                            </div>
                            <div class="your-quiz-result"></div>
                            <div class="col-md-4 ml-auto mr-auto text-center">
                                <br>
                                <a href="index.php" class="btn btn-success btn-round btn-md">Kembali Mendiagnosa</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <script>
                // soal 1
                function yes1() {
                    var a = document.getElementById("card_soal2");
                    var z = document.getElementById("card_soal1");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no1() {
                    var a = document.getElementById("card_soal8");
                    var z = document.getElementById("card_soal1");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }
                // soal 2
                function yes2() {
                    var a = document.getElementById("card_soal3");
                    var z = document.getElementById("card_soal2");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no2() {
                    var a = document.getElementById("card_soal8");
                    var z = document.getElementById("card_soal2");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }

                // soal 3
                function yes3() {
                    var a = document.getElementById("card_soal4");
                    var z = document.getElementById("card_soal3");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no3() {
                    var a = document.getElementById("card_soal11");
                    var z = document.getElementById("card_soal3");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }

                // soal 4
                function yes4() {
                    var a = document.getElementById("card_soal5");
                    var z = document.getElementById("card_soal4");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no4() {
                    var a = document.getElementById("card_soal2");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                    }
                }

                // soal 5
                function yes5() {
                    var a = document.getElementById("card_soal6");
                    var z = document.getElementById("card_soal5");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no5() {
                    var a = document.getElementById("card_soal2");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                    }
                }

                // soal 6
                function yes6() {
                    var a = document.getElementById("card_soal7");
                    var z = document.getElementById("card_soal6");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no6() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal6");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }

                // soal 7
                function yes7() {
                    var a = document.getElementById("card_hasilkatarak");
                    var z = document.getElementById("card_soal7");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no7() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal7");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }

                // soal 8
                function yes8() {
                    var a = document.getElementById("card_soal9");
                    var z = document.getElementById("card_soal8");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no8() {
                    var a = document.getElementById("card_soal16");
                    var z = document.getElementById("card_soal8");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }

                // soal 9
                function yes9() {
                    var a = document.getElementById("card_soal11");
                    var z = document.getElementById("card_soal9");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no9() {
                    var a = document.getElementById("card_soal2");
                    var z = document.getElementById("card_soal9");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 10
                function yes10() {
                    var a = document.getElementById("card_soal12");
                    var z = document.getElementById("card_soal10");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no10() {
                    var a = document.getElementById("card_soal13");
                    var z = document.getElementById("card_soal10");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 11
                function yes11() {
                    var a = document.getElementById("card_soal10");
                    var z = document.getElementById("card_soal11");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no11() {
                    var a = document.getElementById("card_soal13");
                    var z = document.getElementById("card_soal11");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 12
                function yes12() {
                    var a = document.getElementById("card_hasiloveitis");
                    var z = document.getElementById("card_soal12");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no12() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal12");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 13
                function yes13() {
                    var a = document.getElementById("card_soal17");
                    var z = document.getElementById("card_soal13");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no13() {
                    var a = document.getElementById("card_soal14");
                    var z = document.getElementById("card_soal13");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 14
                function yes14() {
                    var a = document.getElementById("card_soal20");
                    var z = document.getElementById("card_soal14");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no14() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal14");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 15
                function yes15() {
                    var a = document.getElementById("card_soal14");
                    var z = document.getElementById("card_soal15");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no15() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal15");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 16
                function yes16() {
                    var a = document.getElementById("card_soal15");
                    var z = document.getElementById("card_soal16");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no16() {
                    var a = document.getElementById("card_soal22");
                    var z = document.getElementById("card_soal16");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 17
                function yes17() {
                    var a = document.getElementById("card_soal18");
                    var z = document.getElementById("card_soal17");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no17() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal17");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 18
                function yes18() {
                    var a = document.getElementById("card_hasilkonjungtivitis");
                    var z = document.getElementById("card_soal18");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no18() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal18");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 19
                function yes19() {
                    var a = document.getElementById("card_hasilglaukoma");
                    var z = document.getElementById("card_soal19");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no19() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal19");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 20
                function yes20() {
                    var a = document.getElementById("card_soal19");
                    var z = document.getElementById("card_soal20");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no20() {
                    var a = document.getElementById("card_soal21");
                    var z = document.getElementById("card_soal20");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 21
                function yes21() {
                    var a = document.getElementById("card_hasilmiopi");
                    var z = document.getElementById("card_soal21");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no21() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal21");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 22
                function yes22() {
                    var a = document.getElementById("card_soal2");
                    var z = document.getElementById("card_soal22");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no22() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal22");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
            </script>
        </main>
    </div>
</body>
<!--   Core JS Files   -->
<script src="asset_user/js/core/jquery.min.js" type="text/javascript"></script>
<script src="asset_user/js/core/popper.min.js" type="text/javascript"></script>
<script src="asset_user/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
<script src="asset_user/js/plugins/moment.min.js"></script>
<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<script src="asset_user/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="asset_user/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
<script src="asset_user/js/material-kit.js?v=2.0.7" type="text/javascript"></script>

<script src="asset_particle/particles.js"></script>
<script src="asset_particle/js/app.js"></script>

<script type="text/javascript" src="slick/slick.min.js"></script>

</html>