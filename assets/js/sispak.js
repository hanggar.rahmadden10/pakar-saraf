 // soal 1
        var message = document.querySelector('#message');

        var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
        var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;

        var grammar = '#JSGF V1.0;'

        var recognition = new SpeechRecognition();
        var speechRecognitionList = new SpeechGrammarList();
        speechRecognitionList.addFromString(grammar, 1);
        recognition.grammars = speechRecognitionList;
        recognition.lang = 'en-US';
        recognition.interimResults = false;

        recognition.onresult = function (event) {
            var last = event.results.length - 1;
            var command = event.results[last][0].transcript;
            // message.textContent = 'Voice Input: ' + command + '.';

            if (command.toLowerCase() == 'yes') {
                pertanyaan2();
            } else if (command.toLowerCase() == 'no') {
                pertanyaan4();
            } else {
                message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
            }
        };

        recognition.onspeechend = function () {
            recognition.stop();
        };

        recognition.onerror = function (event) {
            message.textContent = 'Error occurred in recognition: ' + event.error;
        }

        document.querySelector('#btnGiveCommand').addEventListener('click', function () {
            recognition.start();

        });

        function pertanyaan2() {
            message.textContent = '';
            document.getElementById("pertanyaan").innerHTML =
                "Apakah Mata anda merasakan nyeri hebat ?";
            recognition.onresult = function (event) {
                var last = event.results.length - 1;
                var command = event.results[last][0].transcript;
                if (command.toLowerCase() == 'yes') {
                    pertanyaan3();
                } else if (command.toLowerCase() == 'no') {
                    pertanyaan4();
                } else {
                    message.textContent = 'Sistem tidak dapat mendeteksi suara, coba lagi!';
                }
            };
        }

                // soal 3
                function yes3() {
                    var a = document.getElementById("card_soal4");
                    var z = document.getElementById("card_soal3");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no3() {
                    var a = document.getElementById("card_soal10");
                    var z = document.getElementById("card_soal3");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }

                // soal 4
                function yes4() {
                    var a = document.getElementById("card_soal5");
                    var z = document.getElementById("card_soal4");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no4() {
                    var a = document.getElementById("card_soal11");
                    var z = document.getElementById("card_soal4");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                    }
                }

                // soal 5
                function yes5() {
                    var a = document.getElementById("card_soal6");
                    var z = document.getElementById("card_soal5");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no5() {
                    var a = document.getElementById("card_soal12");
                    var z = document.getElementById("card_soal5");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                    }
                }

                // soal 6
                function yes6() {
                    var a = document.getElementById("card_soal7");
                    var z = document.getElementById("card_soal6");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no6() {
                    var a = document.getElementById("card_soal13");
                    var z = document.getElementById("card_soal6");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }

                // soal 7
                function yes7() {
                    var a = document.getElementById("card_hasilalzheimer");
                    var z = document.getElementById("card_soal7");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no7() {
                    var a = document.getElementById("card_soal14");
                    var z = document.getElementById("card_soal7");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }

                // soal 8
                function yes8() {
                    var a = document.getElementById("card_soal9");
                    var z = document.getElementById("card_soal8");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no8() {
                    var a = document.getElementById("card_soal16");
                    var z = document.getElementById("card_soal8");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }

                // soal 9
                function yes9() {
                    var a = document.getElementById("card_soal10");
                    var z = document.getElementById("card_soal9");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no9() {
                    var a = document.getElementById("card_soal11");
                    var z = document.getElementById("card_soal9");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 10
                function yes10() {
                    var a = document.getElementById("card_soal11");
                    var z = document.getElementById("card_soal10");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no10() {
                    var a = document.getElementById("card_soal13");
                    var z = document.getElementById("card_soal10");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 11
                function yes11() {
                    var a = document.getElementById("card_soal12");
                    var z = document.getElementById("card_soal11");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no11() {
                    var a = document.getElementById("card_soal13");
                    var z = document.getElementById("card_soal11");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 12
                function yes12() {
                    var a = document.getElementById("card_soal13");
                    var z = document.getElementById("card_soal12");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no12() {
                    var a = document.getElementById("card_soal30");
                    var z = document.getElementById("card_soal12");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 13
                function yes13() {
                    var a = document.getElementById("card_soal14");
                    var z = document.getElementById("card_soal13");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no13() {
                    
                    var z = document.getElementById("card_soal13");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 14
                function yes14() {
                    var a = document.getElementById("card_hasilmeningitis");
                    var z = document.getElementById("card_soal14");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no14() {
                    var a = document.getElementById("card_soal21");
                    var z = document.getElementById("card_soal14");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 15
                function yes15() {
                    var a = document.getElementById("card_soal16");
                    var z = document.getElementById("card_soal15");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no15() {
                    var a = document.getElementById("card_soal21");
                    var z = document.getElementById("card_soal15");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 16
                function yes16() {
                    var a = document.getElementById("card_soal17");
                    var z = document.getElementById("card_soal16");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no16() {
                    var a = document.getElementById("card_soal22");
                    var z = document.getElementById("card_soal16");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 17
                function yes17() {
                    var a = document.getElementById("card_soal18");
                    var z = document.getElementById("card_soal17");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no17() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal17");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 18
                function yes18() {
                    var a = document.getElementById("card_soal19");
                    var z = document.getElementById("card_soal18");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no18() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal18");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 19
                function yes19() {
                    var a = document.getElementById("card_soal20");
                    var z = document.getElementById("card_soal19");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no19() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal19");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 20
                function yes20() {
                    var a = document.getElementById("card_soal21");
                    var z = document.getElementById("card_soal20");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no20() {
                    var a = document.getElementById("card_soal21");
                    var z = document.getElementById("card_soal20");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 21
                function yes21() {
                    var a = document.getElementById("card_hasilstroke");
                    var z = document.getElementById("card_soal21");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no21() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal21");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 22
                function yes22() {
                    var a = document.getElementById("card_soal23");
                    var z = document.getElementById("card_soal22");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no22() {
                    var a = document.getElementById("card_soal34");
                    var z = document.getElementById("card_soal22");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 23
                function yes23() {
                    var a = document.getElementById("card_soal24");
                    var z = document.getElementById("card_soal23");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no23() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal23");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 24
                function yes24() {
                    var a = document.getElementById("card_soal25");
                    var z = document.getElementById("card_soal24");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no24() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal24");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 25
                function yes25() {
                    var a = document.getElementById("card_soal26");
                    var z = document.getElementById("card_soal25");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no25() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal25");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 26
                function yes26() {
                    var a = document.getElementById("card_soal27");
                    var z = document.getElementById("card_soal26");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no26() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal26");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 27
                function yes27() {
                    var a = document.getElementById("card_soal28");
                    var z = document.getElementById("card_soal27");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no27() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal27");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 28
                function yes28() {
                    var a = document.getElementById("card_soal29");
                    var z = document.getElementById("card_soal28");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no28() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal28");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 29
                function yes29() {
                    var a = document.getElementById("card_hasilmultiplesclerosis");
                    var z = document.getElementById("card_soal29");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no29() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal29");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 30
                function yes30() {
                    var a = document.getElementById("card_soal31");
                    var z = document.getElementById("card_soal30");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no30() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal30");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 31
                function yes31() {
                    var a = document.getElementById("card_soal32");
                    var z = document.getElementById("card_soal31");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no31() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal31");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 32
                function yes32() {
                    var a = document.getElementById("card_soal33");
                    var z = document.getElementById("card_soal32");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no32() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal32");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 33
                function yes33() {
                    var a = document.getElementById("card_hasilepilepsi");
                    var z = document.getElementById("card_soal33");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no33() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal33");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 34
                function yes34() {
                    var a = document.getElementById("card_soal35");
                    var z = document.getElementById("card_soal34");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no34() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal34");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                 // soal 35
                function yes35() {
                    var a = document.getElementById("card_soal36");
                    var z = document.getElementById("card_soal35");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no35() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal35");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 36
                function yes36() {
                    var a = document.getElementById("card_soal37");
                    var z = document.getElementById("card_soal36");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no36() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal36");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 37
                function yes37() {
                    var a = document.getElementById("card_soal38");
                    var z = document.getElementById("card_soal37");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no37() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal37");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 38
                function yes38() {
                    var a = document.getElementById("card_soal39");
                    var z = document.getElementById("card_soal38");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no38() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal38");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 39
                function yes39() {
                    var a = document.getElementById("card_soal40");
                    var z = document.getElementById("card_soal39");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no39() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal39");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }
                // soal 40
                function yes40() {
                    var a = document.getElementById("card_hasilbellspalsy");
                    var z = document.getElementById("card_soal40");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }

                }

                function no40() {
                    var a = document.getElementById("card_tidakadahasil");
                    var z = document.getElementById("card_soal40");
                    if (a.style.display = "none") {
                        $(a.style.display = "block");
                        $(z.style.display = "none");
                    }
                }